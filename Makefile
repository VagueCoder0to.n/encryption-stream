#!make
GIT_BRANCH := $(shell git rev-parse --abbrev-ref HEAD)
PROJECT_ROOT = ${shell go env GOPATH}/src/gitlab.com/VagueCoder0to.n/Encryption-Stream

proto_files:
	cd ${PROJECT_ROOT}/app/proto && \
	protoc --go_out=cryptpb --go_opt=paths=source_relative \
	--go-grpc_out=cryptpb --go-grpc_opt=paths=source_relative services.proto \
	&& cd ${PROJECT_ROOT}

install:
	- go get -u google.golang.org/grpc \
		google.golang.org/grpc/cmd/protoc-gen-go-grpc \
		google.golang.org/protobuf/proto \
		google.golang.org/protobuf/cmd/protoc-gen-go

server:
	go build -o ${PROJECT_ROOT}/app/server/encryption-stream-server ${PROJECT_ROOT}/app/server/*.go
	${PROJECT_ROOT}/app/server/encryption-stream-server

client:
	go build -o ${PROJECT_ROOT}/app/client/encryption-stream-client ${PROJECT_ROOT}/app/client/*.go
	${PROJECT_ROOT}/app/client/encryption-stream-client

gitlab_push:
	- git add *
	- git add .gitignore
ifdef c
	- @echo ${c}
	- git commit -m "${c}"
else
	- git commit -m "Corrections"
endif
	- git push -u origin ${GIT_BRANCH}

run_temp:
	go run temp/main.go

cp_image:
	WINDOWS=/mnt/i/WSL-Joint
	cp $(PWD)/app/client/image.png_decoded $(WINDOWS)/image_decoded.png

git_merge:
ifeq (,$(and $(filter Changes not staged for commit, $(shell git status)), $(filter Changes to be committed, $(shell git status))))
	- @echo "Changes ommitted/up-to-date for current working branch. Proceeding...";
ifdef to
ifeq (,$(filter $(to), $(GIT_BRANCH)))
ifneq (,$(filter $(to), $(shell git branch)))
	- @echo "Branch '${to}' found. Proceeding...";
	- $(eval CURRENT_BRANCH := $(GIT_BRANCH))
	- @git checkout ${to};
	- @git merge $(CURRENT_BRANCH);
	- @git checkout $(CURRENT_BRANCH);
	- @echo "All changes of '$(CURRENT_BRANCH)' merged with '$(to)'. Back to '$(CURRENT_BRANCH)'.";
else
	- @echo "Exited. Branch '${to}' not found.";
	- @exit 0;
endif
else
	- @echo "Exited. Current branch and merge-to branch cannot be same.";
	- @exit 0;
endif
else
	- @echo "Exited. Provide merge-to branch as to=<branch_name> and retry.";
	- @exit 0;
endif
else
	- @echo "Exited. Please do the add/rm/commit in current branch and retry.";
	- @exit 0;
endif
