module gitlab.com/VagueCoder0to.n/Encryption-Stream/app

go 1.13

require (
	github.com/golang/protobuf v1.5.2 // indirect
	google.golang.org/grpc v1.36.1
	google.golang.org/protobuf v1.26.0
)
