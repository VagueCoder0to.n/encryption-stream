package main

import (
	"log"
	"os"
)

func Write(filepath string, data [][]byte) {
	file, err := os.OpenFile(filepath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0744)
	if err != nil {
		log.Fatalf("Error at file open: %v", err)
	}

	for _, line := range data {
		_, err = file.Write(line)
		if err != nil {
			log.Fatalf("Error writing file: %v", err)
		}
	}
}
