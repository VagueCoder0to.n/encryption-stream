package main

import (
	"bufio"
	"io"
	"log"
	"os"
	"sync"

	"gitlab.com/VagueCoder0to.n/Encryption-Stream/app/proto/cryptpb"
)

// EncryptData takes the plain text, sends to gRPC server and returns encoded text
func (c *Client) EncryptData(sc cryptpb.CryptServiceClient, data []string) [][]byte {
	c.Logger.Printf("gRPC Client is Streaming Encrypt")

	encodedText := [][]byte{}
	waitc := make(chan struct{})
	mu := &sync.Mutex{}

	go func() {
		for {
			res, err := c.encryptStream.Recv()
			if err == io.EOF {
				close(waitc)
				break
			}
			if err != nil {
				c.Logger.Fatalf("Error in Client stream Recv: %v", err)
			}
			encoded := res.GetEncodedText()
			c.Logger.Println("Encrypt | Received from server")

			mu.Lock()
			encodedText = append(encodedText, encoded)
			mu.Unlock()
		}
	}()

	waitc2 := make(chan struct{})

	go c.ReadAndEncrypt(waitc2, "./app/client/image.png", 8)

	<-waitc2
	c.encryptStream.CloseSend()

	<-waitc
	return encodedText
}

func (c *Client) ReadAndEncrypt(wc chan struct{}, filepath string, size int) {
	file, err := os.OpenFile(filepath, os.O_RDONLY, 0444)
	if err != nil {
		log.Fatalf("Error at file open: %v", err)
	}

	reader := bufio.NewReaderSize(file, size)

	var ln, pn int32
	for {
		line, continueLine, err := reader.ReadLine()
		if err != nil {
			if err == io.EOF {
				break
			}
			log.Fatalf("Error at read line: %v", err)
		}

		err = c.encryptStream.Send(&cryptpb.EncryptRequest{
			PlainText: line,
			Order: &cryptpb.Info{
				Line: ln,
				Part: pn,
			},
		})
		if err != nil {
			c.Logger.Fatalf("Error in Client stream Send: %v", err)
		}

		if !continueLine {
			ln++
			pn = 0
			// line = append(line, []byte("\n")...)
		} else {
			pn++
		}

		c.Logger.Println("Encrypt | Sent to server")
	}
	close(wc)
}
