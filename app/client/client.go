package main

import (
	"context"
	"log"
	"os"

	"gitlab.com/VagueCoder0to.n/Encryption-Stream/app/proto/cryptpb"
	"google.golang.org/grpc"
)

type Client struct {
	cryptpb.UnimplementedCryptServiceServer
	Logger        *log.Logger
	encryptStream cryptpb.CryptService_EncryptClient
	decryptStream cryptpb.CryptService_DecryptClient
}

func main() {
	logger := log.New(os.Stderr, "[Encryption-Stream Client] ", log.Lshortfile|log.LstdFlags)

	conn, err := grpc.Dial(":50051", grpc.WithInsecure())
	if err != nil {
		logger.Fatalf("Error at grpc Dial: %v", err)
	}
	defer conn.Close()

	serviceClient := cryptpb.NewCryptServiceClient(conn)
	es, err := serviceClient.Encrypt(context.Background())
	if err != nil {
		logger.Fatalf("Error at encrypt-stream creation: %v", err)
	}

	ds, err := serviceClient.Decrypt(context.Background())
	if err != nil {
		logger.Fatalf("Error at decrypt-stream creation: %v", err)
	}

	client := &Client{
		Logger:        logger,
		encryptStream: es,
		decryptStream: ds,
	}

	encoded := client.EncryptData(serviceClient, getData())
	logger.Printf("Encoded Data: %v", encoded)
	Write("./app/client/image.png_encoded", encoded)

	// logger.Fatal("Complete")

	decoded := client.DecryptData(serviceClient, getData())
	logger.Printf("Decoded Data: %v", decoded)
	decoded[len(decoded)-1] = append(decoded[len(decoded)-1], []byte("\n")...)
	Write("./app/client/image.png_decoded", decoded)
}
