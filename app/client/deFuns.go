package main

import (
	"bufio"
	"io"
	"log"
	"os"
	"sync"

	"gitlab.com/VagueCoder0to.n/Encryption-Stream/app/proto/cryptpb"
)

// DecryptData takes the encoded text, sends to gRPC server and returns plain text
func (c *Client) DecryptData(sc cryptpb.CryptServiceClient, data []string) [][]byte {
	c.Logger.Printf("gRPC Client is Streaming Decrypt")

	decodedText := [][]byte{}
	line := []byte{}
	waitc := make(chan struct{})
	mu := &sync.Mutex{}

	go func() {
		lno := int32(0)
		for {
			res, err := c.decryptStream.Recv()
			if err == io.EOF {
				mu.Lock()
				decodedText = append(decodedText, line)
				mu.Unlock()

				close(waitc)
				break
			}
			if err != nil {
				c.Logger.Fatalf("Error in Client stream Recv: %v", err)
			}
			decoded := res.GetPlainText()
			c.Logger.Println("Decrypt | Received from server")

			currentLno := res.GetOrder().GetLine()
			if lno == currentLno {
				line = append(line, decoded...)
			} else {
				line = append(line, []byte("\n")...)
				mu.Lock()
				decodedText = append(decodedText, line)
				mu.Unlock()
				line = decoded
				lno = currentLno
			}
		}
	}()

	waitc2 := make(chan struct{})

	go c.ReadAndDecrypt(waitc2, "./app/client/image.png_encoded")

	<-waitc2
	c.decryptStream.CloseSend()

	<-waitc
	return decodedText
}

func (c *Client) ReadAndDecrypt(wc chan struct{}, filepath string) {
	file, err := os.OpenFile(filepath, os.O_RDONLY, 0444)
	if err != nil {
		log.Fatalf("Error at file open: %v", err)
	}

	reader := bufio.NewReader(file)

	for {
		line, _, err := reader.ReadLine()
		if err != nil {
			if err == io.EOF {
				break
			}
			log.Fatalf("Error at read line: %v", err)
		}

		err = c.decryptStream.Send(&cryptpb.DecryptRequest{
			EncodedText: line,
		})

		if err != nil {
			c.Logger.Fatalf("Error in Client stream Send: %v", err)
		}

		c.Logger.Println("Decrypt | Sent to server")

	}

	close(wc)
}
