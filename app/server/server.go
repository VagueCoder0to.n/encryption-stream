package main

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net"
	"os"

	"gitlab.com/VagueCoder0to.n/Encryption-Stream/app/proto/cryptpb"
	"google.golang.org/grpc"
)

type Server struct {
	cryptpb.UnimplementedCryptServiceServer
	Logger *log.Logger
}

func main() {
	logger := log.New(os.Stderr, "[Encryption-Stream Server] ", log.Lshortfile|log.LstdFlags)

	lis, err := net.Listen("tcp", ":50051")
	if err != nil {
		logger.Fatalf("Failed at net Listen: %v", err)
	}

	grpcServer := grpc.NewServer()
	cryptpb.RegisterCryptServiceServer(grpcServer, &Server{
		Logger: logger,
	})

	logger.Printf("gRPC Server is Serving on port 50051")
	if err := grpcServer.Serve(lis); err != nil {
		logger.Fatalf("Failed at grpc Serve: %v", err)
	}
}

func (s *Server) Encrypt(stream cryptpb.CryptService_EncryptServer) error {
	for {
		r, err := stream.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			return fmt.Errorf("Error at Encrypt stream Recv: %v", err)
		}

		fulltext := bytes.Join(
			[][]byte{
				[]byte(fmt.Sprint(r.GetOrder().Line)),
				[]byte(fmt.Sprint(r.GetOrder().Part)),
				r.GetPlainText(),
			},
			[]byte("+++"),
		)
		fulltext = append(fulltext, []byte("\n")...)

		s.Logger.Printf("Encrypt | Received Line:%b", fulltext)

		err = stream.Send(&cryptpb.EncryptResponse{
			EncodedText: fulltext,
		})
		if err != nil {
			return fmt.Errorf("Error at Encrypt stream Send: %v", err)
		}
	}
}

func (*Server) Decrypt(stream cryptpb.CryptService_DecryptServer) error {
	for {
		r, err := stream.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			return fmt.Errorf("Error at Decrypt stream Recv: %v", err)
		}

		encoded := r.GetEncodedText()

		decoded, line, part := []byte("\n"), int32(0), int32(0)
		if bytes.Compare(encoded, []byte{}) != 0 {
			decoded, line, part, err = DecryptText(encoded)
			if err != nil {
				return fmt.Errorf("Error from decrypton: %v", err)
			}
			err = stream.Send(&cryptpb.DecryptResponse{
				PlainText: decoded,
				Order: &cryptpb.Info{
					Line: line,
					Part: part,
				},
			})
			if err != nil {
				return fmt.Errorf("Error at Decrypt stream Send: %v", err)
			}
		}
	}
}
