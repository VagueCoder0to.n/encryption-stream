package main

import (
	"bytes"
	"fmt"
	"strconv"
)

func DecryptText(text []byte) ([]byte, int32, int32, error) {
	fmt.Println(text)
	strs := bytes.Split(text, []byte("+++"))
	if len(strs) < 3 {
		return nil, 0, 0, fmt.Errorf("Error couldn't unzip enough values")
	}

	line, err := strconv.Atoi(string(strs[0]))
	if err != nil {
		return nil, 0, 0, fmt.Errorf("Error couldn't convert line number string to integer")
	}

	part, err := strconv.Atoi(string(strs[1]))
	if err != nil {
		return nil, 0, 0, fmt.Errorf("Error couldn't convert part number string to integer")
	}

	newline := []byte("\n")
	lastIndex := len(strs) - 1
	strs[lastIndex] = bytes.Join(bytes.Split(strs[lastIndex], newline), newline)

	return bytes.Join(strs[2:], []byte("+++")), int32(line), int32(part), nil
}
